# Automatic Semantic Release

Node module for automatic semantic release. Git tags and NPM version updates are generated automatically based on commit message format (which must adhere to Angular JS semantic release commit message conventions).