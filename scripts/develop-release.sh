#!/bin/bash

# Get release type
release=$($(dirname $0)/get-release-type.sh);

# If this is the first commit of a development branch, increase future release depending on commit message
# The first commit of a development branch can be identified because it will contain a non-development version
if [[ $($(dirname $0)/is-development-version.sh) == 0 ]] ; then
  # Patch (Subminor) release
  if [[ $release == 0 ]] ; then
    npm version prepatch --no-git-tag-version;
  # Feature (Minor) release
  elif [[ $release == 1 ]] ; then
    npm version preminor --no-git-tag-version;
  # Breaking (Major) release
  elif [[ $release == 2 ]] ; then
    npm version premajor --no-git-tag-version;
  else
    $(dirname $0)/display-format.sh;
    exit -1;
  fi
fi

# Increase development release
npm version prerelease --no-git-tag-version;
