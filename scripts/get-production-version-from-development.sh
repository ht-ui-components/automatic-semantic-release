#!/bin/bash

# Extract production version from development one
current_version=$($(dirname $0)/get-current-version.sh);
production_version_extraction_regexp="[^-]+";

# Execute regular expresion to extract production part from development version
[[ $current_version =~ $production_version_extraction_regexp ]];
if [[ ${#BASH_REMATCH[@]} != 1 ]] ; then
  echo -e "\e[1;31mGot an unexpected error while trying to get production version from development one\e[0m";
  exit -1;
fi
echo ${BASH_REMATCH[0]};
