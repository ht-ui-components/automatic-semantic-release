#!/bin/bash

# Check if current version comes from development (Example: 1.2.0-2)
current_version=$($(dirname $0)/get-current-version.sh);
development_version_regexp=".*-.*";

# Check if current version comes from development
[[ $current_version =~ $development_version_regexp ]];

if [[ ${#BASH_REMATCH[@]} == 1 ]] ; then
  echo 1;
else
  echo 0;
fi

exit 0;

