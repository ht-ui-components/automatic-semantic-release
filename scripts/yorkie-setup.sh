#!/bin/bash

# Modify parent project's package.json to include yorkie git hook configuration as well as setting up main branch for semantic relase
tmp=$(mktemp);
# Setup main branch to default value (master)
jq '.release.branch = "master"' $(pwd)/../../package.json > "$tmp" && mv "$tmp" $(pwd)/../../package.json;
# Set yorkie git hooks for semantic release depending on commit messages
jq '.gitHooks["commit-msg"] = "./node_modules/automatic-semantic-release/scripts/check-msg.sh"' $(pwd)/../../package.json > "$tmp" && mv "$tmp" $(pwd)/../../package.json;
jq '.gitHooks["post-commit"] = "./node_modules/automatic-semantic-release/scripts/release.sh"' $(pwd)/../../package.json > "$tmp" && mv "$tmp" $(pwd)/../../package.json;
# Set yorkie git hooks for release tag push to remote
jq '.gitHooks["pre-push"] = "./node_modules/automatic-semantic-release/scripts/push-release.sh"' $(pwd)/../../package.json > "$tmp" && mv "$tmp" $(pwd)/../../package.json;
