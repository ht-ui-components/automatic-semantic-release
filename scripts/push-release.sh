#!/bin/bash

# Avoid creating push loops when pushing tags to remote
if [ $SKIP_PUSH ] ; then
  export SKIP_PUSH=0;
  exit 0;
else
  echo -e "\e[1;36mPushing version tags to remote\e[0m";
fi

# Push version tags to remote
export SKIP_PUSH=1;
git push --tags;
