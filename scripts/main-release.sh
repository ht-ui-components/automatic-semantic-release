#!/bin/bash

# If current version comes from development, simply make it a production one
if [[ $($(dirname $0)/is-development-version.sh) == 1 ]] ; then
  echo -e "\e[1;36mGetting new stable release from development version\e[0m";
  npm version $($(dirname $0)/get-production-version-from-development.sh)  --no-git-tag-version;
  exit 0;
fi

# Get release type
release=$($(dirname $0)/get-release-type.sh);

# Increase release depending on commit message (using Angular's syntax for release)
# Patch (Subminor) release
if [[ $release == 0 ]] ; then
  npm version patch --no-git-tag-version;
# Feature (Minor) release
elif [[ $release == 1 ]] ; then
  npm version minor --no-git-tag-version;
# Breaking (Major) release
elif [[ $release == 2 ]] ; then
  npm version major --no-git-tag-version;
else
  $(dirname $0)/display-format.sh;
  exit -1;
fi
