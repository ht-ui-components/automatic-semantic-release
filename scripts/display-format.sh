#!/bin/bash

echo -e "\e[1;31mInvalid commit message. You need to use the following syntax:\e[0m";
echo -e "\e[1;31m[#<task_id>] <commit_objective>[(<scope>)]: <message>\e[0m";
echo -e "\e[1;31m> task_id (optional): Task to which this commit is refering to\e[0m";
echo -e "\e[1;31m> commit_objetive: String indicating the objective of the present commit. Must be one of the following:\e[0m";
echo -e "\e[1;31m        fix: for a bug fix\e[0m";
echo -e "\e[1;31m        feat: for a new feature\e[0m";
echo -e "\e[1;31m        docs: for changes related to the documentation of the project\e[0m";
echo -e "\e[1;31m        style: for changes that don't affect the meaning of the code (such as white spaces, formatting or semi-colons)\e[0m";
echo -e "\e[1;31m        refactor: for code improvements that don't fix bugs or add features, but improves maintainability\e[0m";
echo -e "\e[1;31m        test: for updates related to tests\e[0m";
echo -e "\e[1;31m        chore: changes to the build process\e[0m";
echo -e "\e[1;31m        BREAKING CHANGE: changes without retro-compatibility\e[0m";
echo -e "\e[1;31m> scope (optional): scope of the changes made, for example: 'connectivity, geolocation, etc'\e[0m";
echo -e "\e[1;31m> message: commit message describing the changes made. Ideally expressed in present, such as 'add new sort method', instead of 'added new sort method'\e[0m";
echo -e "\e[1;31mExample: #45 fix(node dashboard): fix node installation process\e[0m";
