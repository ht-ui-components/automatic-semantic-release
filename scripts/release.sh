#!/bin/bash

# Define main branch
main_branch=$(cat $(pwd)/package.json | jq -r ".release.branch");

# Make sure main branch has been correctly detected
if [ $? -ne 0 ] ; then
  echo -e "\e[1;31mSemantic release error: git main branch could not be detected. Please make sure you have 'jq' package installed on your system\e[0m";
  exit -1;
fi

# Avoid creating commit loops when ammending (which is necessary for increasing version)
if [ $SKIP_RELEASE ] ; then
  export SKIP_REPEASE=0;
  exit 0;
else
  echo -e "\e[1;36mGot main branch: $main_branch\e[0m";
fi

# Determine the release type (main or develop) depending on current branch
current_branch=$(git rev-parse --abbrev-ref HEAD);
echo -e "\e[1;36mGot current branch: $current_branch\e[0m";
if [ "$current_branch" == "$main_branch" ] ; then
  echo -e "\e[1;92mTriggering release\e[0m";
  $(dirname $0)/main-release.sh;
else
  echo -e "\e[1;93mTriggering develop release\e[0m";
  $(dirname $0)/develop-release.sh;
fi

if [ $? -ne 0 ] ; then
  exit -1;
fi

# Update version and modify present commit to include said change
git add -u package.json;
git add -u package-lock.json;
export SKIP_RELEASE=1;
git commit --amend --no-verify -C HEAD;

# Add version tag
new_version=$(cat $(pwd)/package.json | jq -r ".version");
git tag $new_version;
