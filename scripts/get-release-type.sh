#!/bin/bash

# Define release types
subminor_regexp="^(#[0-9]* )?((fix))(\(.+\))?: .+$";
minor_regexp="^(#[0-9]* )?((feat)|(docs)|(style)|(refactor)|(test)|(chore))(\(.+\))?: .+$";
major_regexp="^(#[0-9]* )?((BREAKING CHANGE))(\(.+\))?: .+$";

# Determine release type depending on commit message (using Angular's syntax for release)
# Patch (Subminor) release
commit_msg=$(cat .git/COMMIT_EDITMSG);
if [[ $commit_msg =~ $subminor_regexp ]] ; then
  echo 0;
# Feature (Minor) release
elif [[ $commit_msg =~ $minor_regexp ]] ; then
  echo 1;
# Breaking (Major) release
elif [[ $commit_msg =~ $major_regexp ]] ; then
  echo 2;
else
  exit -1;
fi
